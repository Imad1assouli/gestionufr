package com.GestionDesUfr.GestionDesUfr;

import com.GestionDesUfr.GestionDesUfr.Models.Classe;
import com.GestionDesUfr.GestionDesUfr.Models.Eleve;
import com.GestionDesUfr.GestionDesUfr.Repository.RepositoryClasse;
import com.GestionDesUfr.GestionDesUfr.Repository.RepositoryEleve;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class GestionDesUfrApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionDesUfrApplication.class, args);
	}

	@Autowired
	private RepositoryClasse repositoryClasse;
	@Autowired
	private RepositoryEleve repositoryEleve;
	@Bean
	CommandLineRunner start(){
		return args -> {
			//Classe classe=new Classe("Informatique","Licence",null,null);
			//repositoryClasse.save(classe);

			/*
			Optional<Classe> classeOptional = repositoryClasse.findById("Informatique");
			if (classeOptional.isPresent()) {
				Classe classe = classeOptional.get();
				classe.setCycleClasse("Master");
				repositoryClasse.save(classe);
			}

			 */


			//Eleve eleve=new Eleve(null,"Assouli","Imad","23-06-2001",null);
			//repositoryEleve.save(eleve);

			List<Classe> classes = repositoryClasse.findClassesByCycleClasse("master");
			System.out.println("Console here:");
			classes.forEach(classe -> {
				System.out.println(classe.getNomClasse());
			});
			System.out.println("Console here:");
			Eleve eleve=repositoryEleve.findEleveBynomEleve("Assouli");
			System.out.println(eleve.getNoMatricule());











		};
	}



}
