package com.GestionDesUfr.GestionDesUfr.Models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Classe {
    @Id
    private String nomClasse;
    private String cycleClasse;

    @OneToMany(mappedBy = "classe")
    Set<Frequenter> anneeScolaire;

    @OneToMany(mappedBy = "classe")
    private Set<Enseigner> annee;


}
