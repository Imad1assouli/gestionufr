package com.GestionDesUfr.GestionDesUfr.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Eleve {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long noMatricule;
    private String nomEleve;
    private String prenomEleve;
    private String dateNaissance;
    @OneToMany(mappedBy = "eleve")
    private Set<Frequenter> anneeScolaire;
}
