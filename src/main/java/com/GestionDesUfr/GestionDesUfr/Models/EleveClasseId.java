package com.GestionDesUfr.GestionDesUfr.Models;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class EleveClasseId implements Serializable {
    private Long noMatricule;
    private String nomClasse;
}
