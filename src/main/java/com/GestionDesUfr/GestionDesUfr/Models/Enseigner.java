package com.GestionDesUfr.GestionDesUfr.Models;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;

@Entity
public class Enseigner {
    @EmbeddedId
    private ProfMatiereClasseId id;
    @ManyToOne
    @MapsId("codeProf")
    private Professeur professeur;
    @ManyToOne
    @MapsId("codeMatiere")
    private Matiere matiere;

    @ManyToOne
    @MapsId("nomClasse")
    private Classe classe;
    private String annee;
}
