package com.GestionDesUfr.GestionDesUfr.Models;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;

@Entity
public class Frequenter {
    @EmbeddedId
    private EleveClasseId id;

    @ManyToOne
    @MapsId("noMatricule")
    private Eleve eleve;

    @ManyToOne
    @MapsId("nomClasse")
    private Classe classe;

    private String anneeScolaire;
}
