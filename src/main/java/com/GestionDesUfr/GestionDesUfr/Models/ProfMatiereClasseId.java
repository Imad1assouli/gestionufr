package com.GestionDesUfr.GestionDesUfr.Models;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class ProfMatiereClasseId implements Serializable {
    private Long codeProf;
    private Long codeMatiere;
    private String nomClasse;
}
