package com.GestionDesUfr.GestionDesUfr.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Professeur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeProf;
    private String nom;
    private String prenom;
    @OneToMany(mappedBy = "professeur")
    private Set<Enseigner> Annee;


}
