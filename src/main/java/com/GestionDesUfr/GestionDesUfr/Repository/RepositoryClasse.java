package com.GestionDesUfr.GestionDesUfr.Repository;

import com.GestionDesUfr.GestionDesUfr.Models.Classe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepositoryClasse extends JpaRepository<Classe,String> {
    List<Classe> findClassesByCycleClasse(String cycle);

    /*
    @Query("SELECT c FROM Classe c WHERE c.cycleClasse=?1")
    List<Classe> findClassesByCycle(String cycle);

     */

}
