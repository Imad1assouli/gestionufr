package com.GestionDesUfr.GestionDesUfr.Repository;

import com.GestionDesUfr.GestionDesUfr.Models.Eleve;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryEleve extends JpaRepository<Eleve,Long> {
    Eleve findEleveBynomEleve(String nom);


}
