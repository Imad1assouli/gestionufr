package com.GestionDesUfr.GestionDesUfr.Repository;

import com.GestionDesUfr.GestionDesUfr.Models.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryMatiere extends JpaRepository<Matiere,Long> {
}
