package com.GestionDesUfr.GestionDesUfr.Repository;

import com.GestionDesUfr.GestionDesUfr.Models.Professeur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryProfesseur extends JpaRepository<Professeur,Long> {
}
